// Copyright (c) 2020-present, INSPUR Co, Ltd. All rights reserved.
// This source code is licensed under Apache 2.0 License.

#include <algorithm>

#include <string>
#include <map>

#include "port/stack_trace.h"
#include "db/db_test_util.h"
#include "db/column_family.h"
#include "db/db_impl.h"
#include "db/flush_job.h"
#include "pure_mem/memoryblock/memory_arena/memory_arena.h"
#include "pure_mem/std_out_logger.h"
#include "rocksdb/cache.h"
#include "rocksdb/env.h"
#include "rocksdb/slice.h"
#include "util/file_reader_writer.h"
#include "util/testharness.h"
#include "util/testutil.h"

namespace rocksdb {

    void EncodeUint32(std::string* buf, uint32_t v) {
        const uint8_t tmp[sizeof(v)] = {
                uint8_t(v >> 24),
                uint8_t(v >> 16),
                uint8_t(v >> 8),
                uint8_t(v),
        };
        buf->append(reinterpret_cast<const char*>(tmp), sizeof(tmp));
    }


    void EncodeUint64(std::string* buf, uint64_t v) {
        const uint8_t tmp[sizeof(v)] = {
                uint8_t(v >> 56), uint8_t(v >> 48), uint8_t(v >> 40), uint8_t(v >> 32),
                uint8_t(v >> 24), uint8_t(v >> 16), uint8_t(v >> 8),  uint8_t(v),
        };
        buf->append(reinterpret_cast<const char*>(tmp), sizeof(tmp));
    }

    void EncodeTimestamp(std::string& s, int64_t wall_time, int32_t logical) {
        EncodeUint64(&s, uint64_t(wall_time));
        if (logical != 0) {
            EncodeUint32(&s, uint32_t(logical));
        }
    }
    const int kMVCCVersionTimestampSize = 12;
    std::string EncodeKey(const rocksdb::Slice& key, int64_t wall_time, int32_t logical) {
        std::string s;
        const bool ts = wall_time != 0 || logical != 0;
        s.reserve(key.size() + 1 + (ts ? 1 + kMVCCVersionTimestampSize : 0));
        s.append(key.data(), key.size());
        if (ts) {
            // Add a NUL prefix to the timestamp data. See DBPrefixExtractor.Transform
            // for more details.
            s.push_back(0);
            EncodeTimestamp(s, wall_time, logical);
        }
        s.push_back(char(s.size() - key.size()));
        return s;
    }

const size_t kApproximateSize = 10 << 20;
const size_t kSst_size = 1 << 15;

class FlushL0CacheTest : public DBTestBase {
 public:
  FlushL0CacheTest() : DBTestBase("/db_basic_test"), env_options_(last_options_) {
    last_options_.info_log.reset(new StdOutLogger(InfoLogLevel::DEBUG_LEVEL));
    FlushL0CacheTryReopen();
  }

  Status FlushL0CacheTryReopen() {
    Close();
    return DB::Open(last_options_, dbname_, &db_);
  }

  EnvOptions env_options_;

  static Slice genMVCCKey1(const char* key) {
    uint32_t keySize = strlen(key) + 1;
    char* ret = new char[keySize];
    memset(ret, '\0', keySize);
    memcpy(ret, key, strlen(key));
    return Slice(ret, keySize);
  };

  static Slice CreateInternalKeySlice(SequenceNumber seq);
  static void InsertData(MemArena *memory_arena, int size);
  static std::map<std::string, std::string> InsertDataAndDelete(MemArena *memory_arena, int data_size,
                           int del_nums, int next_key_nums);
  static void InsertDataAndRangeDel(MemArena *memory_arena, int data_size,
                             int range_del_nums, int range_del_size,
                             int next_key_nums, SequenceNumber range_del_seq);

};

Slice FlushL0CacheTest::CreateInternalKeySlice(SequenceNumber seq) {
  std::string a = std::to_string(seq) + "_test";
  Slice key = Slice(genMVCCKey1(a.data()));

  auto key_size = static_cast<uint32_t>(key.size());
  uint32_t internal_key_size = key_size + 8;

  char *buf = new char[internal_key_size];
  char *p = buf;
  memcpy(p, key.data(), key_size);
  p += key_size;
  uint64_t packed = PackSequenceAndType(seq, kTypeValue);
  EncodeFixed64(p, packed);
  return {buf, internal_key_size};
}

void FlushL0CacheTest::InsertData(MemArena *memory_arena, int size = 10) {
  char tmp[1000] = {'a'};
  Slice value(tmp, 1000);
  SequenceNumber s = 100000;
  for (int i = 0; i < size; i++) {
    s++;
    std::string a = std::to_string(s) + "_test";
    Slice key = Slice(genMVCCKey1(a.data()));

    auto key_size = static_cast<uint32_t>(key.size());
    uint32_t internal_key_size = key_size + 8;

    char buf[internal_key_size];
    char *p = buf;
    memcpy(p, key.data(), key_size);
    p += key_size;
    uint64_t packed = PackSequenceAndType(s, kTypeValue);
    EncodeFixed64(p, packed);

    Slice key_slice(buf, internal_key_size);

    // 内存存储区插入数据
    memory_arena->KVDataInsert(key_slice, value);
  }
}

std::map<std::string, std::string> FlushL0CacheTest::InsertDataAndDelete(MemArena *memory_arena, int data_size = 10,
                         int del_nums = 1, int next_key_nums = 5) {
  char tmp[1000] = {'a'};
  Slice value(tmp, 1000);
  SequenceNumber data_seq = 100000;
  SequenceNumber del_seq = 100004;
  std::map<std::string, std::string> kv_map;

  // wangzekun01: 插入数据
  for (int i = 0; i < data_size; i++) {
    ++data_seq;
    std::string a = std::to_string(data_seq) + "_test";
    Slice key = Slice(genMVCCKey1(a.data()));

    auto key_size = static_cast<uint32_t>(key.size());
    uint32_t internal_key_size = key_size + 8;

    char buf[internal_key_size];
    char *p = buf;
    memcpy(p, key.data(), key_size);
    p += key_size;
    uint64_t packed = PackSequenceAndType(data_seq, kTypeValue);
    EncodeFixed64(p, packed);

    Slice key_slice(buf, internal_key_size);

    // 内存存储区插入数据
    memory_arena->KVDataInsert(key_slice, value);
    kv_map[key_slice.ToString()] = value.ToString();
  }

  // wangzekun01: 插入删除数据
  for (int i = 0; i < del_nums; i++) {
    std::string start_key = std::to_string(++del_seq) + "_test";

    ++data_seq;
    Slice key = Slice(genMVCCKey1(start_key.data()));
    auto key_size = static_cast<uint32_t>(key.size());
    uint32_t internal_key_size = key_size + 8;
    char buf[internal_key_size];
    char *p = buf;
    memcpy(p, key.data(), key_size);
    p += key_size;
    uint64_t packed = PackSequenceAndType(data_seq, kTypeDeletion);
    EncodeFixed64(p, packed);
    Slice key_slice(buf, internal_key_size);

    Slice value_slice;
    // 内存存储区插入范围删除数据
    memory_arena->KVDataInsert(key_slice, value_slice);
    kv_map[key_slice.ToString()] = value_slice.ToString();

    del_seq += next_key_nums - 1;
  }

  // 向内存存储区插入一条已删除数据
  {
    ++data_seq;
    int tmp_seq = next_key_nums * 2 + 1 + 100000;
    std::string a = std::to_string(tmp_seq) + "_test";
    Slice key = Slice(genMVCCKey1(a.data()));

    auto key_size = static_cast<uint32_t>(key.size());
    uint32_t internal_key_size = key_size + 8;

    char buf[internal_key_size];
    char *p = buf;
    memcpy(p, key.data(), key_size);
    p += key_size;
    uint64_t packed = PackSequenceAndType(data_seq, kTypeValue);
    EncodeFixed64(p, packed);

    Slice key_slice(buf, internal_key_size);

    // 内存存储区插入数据
    memory_arena->KVDataInsert(key_slice, value);
    kv_map[key_slice.ToString()] = value.ToString();
  }
  return kv_map;
}

void FlushL0CacheTest::InsertDataAndRangeDel(MemArena *memory_arena, int data_size = 10,
                           int range_del_nums = 1, int range_del_size = 2,
                           int next_key_nums = 5, SequenceNumber range_del_seq = 100000) {

  char tmp[1000] = {'a'};
  Slice value(tmp, 1000);
  SequenceNumber data_seq = 100000;

  // wangzekun01: 插入数据
  for (int i = 0; i < data_size; i++) {
    data_seq++;
    std::string a = std::to_string(data_seq) + "_test";
    Slice key = Slice(genMVCCKey1(a.data()));

    auto key_size = static_cast<uint32_t>(key.size());
    uint32_t internal_key_size = key_size + 8;

    char buf[internal_key_size];
    char *p = buf;
    memcpy(p, key.data(), key_size);
    p += key_size;
    uint64_t packed = PackSequenceAndType(data_seq, kTypeValue);
    EncodeFixed64(p, packed);

    Slice key_slice(buf, internal_key_size);

    // 内存存储区插入数据
    memory_arena->KVDataInsert(key_slice, value);
  }

  // wangzekun01: 插入范围删除数据
  for (int i = 0; i < range_del_nums; i++) {
    // wangzekun01: 获取范围删除的start key和end key。
    std::string start_key = std::to_string(++range_del_seq) + "_test";
    std::string end_key = std::to_string(range_del_seq + range_del_size) + "_test";

    ++data_seq;
    Slice key = Slice(genMVCCKey1(start_key.data()));
    auto key_size = static_cast<uint32_t>(key.size());
    uint32_t internal_key_size = key_size + 8;
    char buf[internal_key_size];
    char *p = buf;
    memcpy(p, key.data(), key_size);
    p += key_size;
    uint64_t packed = PackSequenceAndType(data_seq, kTypeRangeDeletion);
    EncodeFixed64(p, packed);
    Slice key_slice(buf, internal_key_size);

    Slice value_slice(genMVCCKey1(end_key.data()));

    // 内存存储区插入范围删除数据
    memory_arena->RangeDeletionInsert(key_slice, value_slice);

    range_del_seq += next_key_nums - 1;
  }
}

TEST_F(FlushL0CacheTest, BasicFlush) {

  // 为默认列族添加内存存储区和数据
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  ColumnFamilyData *default_cfd = cfh->cfd();
  Options options = Options();
  ImmutableCFOptions ioptions(options);
  ioptions.encode_version_node = false;
  auto memory_arena = new MemArena(default_cfd->internal_comparator(), ioptions);
//  auto memory_arena = new MemArena(default_cfd->internal_comparator(), *default_cfd->ioptions());
  InsertData(memory_arena);
  memory_arena->Ref();
  default_cfd->SetMemoryarena(memory_arena);

  // 创建其他所需参数
  JobContext job_context(0);
  MutableCFOptions mutable_cf_options = *default_cfd->GetLatestMutableCFOptions();
  std::vector<SequenceNumber> snapshot_seqs;
  SequenceNumber earliest_write_conflict_snapshot;
  SnapshotChecker *snapshot_checker;
  VersionEdit edit;;
  std::vector<FileMetaData> metas;
  dbfull()->mutex()->Lock();
  dbfull()->GetSnapshotContext(&job_context, &snapshot_seqs,
                               &earliest_write_conflict_snapshot, &snapshot_checker);
  std::vector<Slice> empty_boundaries;
  // 写入数据
  Status s = dbfull()->FlushL0CacheToSST(default_cfd, mutable_cf_options, &job_context,
                                         snapshot_seqs, earliest_write_conflict_snapshot,
                                         snapshot_checker, Env::HIGH, kApproximateSize, empty_boundaries,
                                         edit, metas);
  s = default_cfd->current()->version_set()->LogAndApply(default_cfd, *default_cfd->GetLatestMutableCFOptions(),
                                                         &edit, dbfull()->mutex());
  dbfull()->mutex()->Unlock();

  // Manifest检查
  const auto &l1_files = default_cfd->current()->storage_info()->LevelFiles(1);
  ASSERT_EQ(l1_files.size(), 1);
  ASSERT_EQ(genMVCCKey1((std::to_string(100001) + "_test").data()), l1_files[0]->smallest.user_key());
  ASSERT_EQ(genMVCCKey1((std::to_string(100010) + "_test").data()), l1_files[0]->largest.user_key());

  // SST数据插入检查
  ReadRangeDelAggregator range_del_aggregator(&default_cfd->internal_comparator(), kMaxSequenceNumber);
  std::unique_ptr<InternalIterator> it(default_cfd->table_cache()->NewIterator(
      ReadOptions(), env_options_, default_cfd->internal_comparator(), *l1_files[0],
      &range_del_aggregator /* range_del_agg */, mutable_cf_options.prefix_extractor.get(),
      nullptr, nullptr, true /* for_compaction */, nullptr /* arena */,
      false /* skip_filter */, 1));
  int seq = 100000;
  for (it->SeekToFirst(); it->Valid(); it->Next()) {
    ++seq;
    ASSERT_EQ(it->user_key(), genMVCCKey1((std::to_string(seq) + "_test").data()));
    ASSERT_EQ(it->value().ToString()[0], 'a');
  }
}

TEST_F(FlushL0CacheTest, SstSizeControl) {

  // 为默认列族添加内存存储区和数据
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  ColumnFamilyData *default_cfd = cfh->cfd();
  Options options = Options();
  ImmutableCFOptions ioptions(options);
  ioptions.encode_version_node = false;
  auto memory_arena = new MemArena(default_cfd->internal_comparator(), ioptions);
//  auto memory_arena = new MemArena(default_cfd->internal_comparator(), *default_cfd->ioptions());
  InsertData(memory_arena, 100);
  memory_arena->Ref();
  default_cfd->SetMemoryarena(memory_arena);

  // 创建其他所需参数
  JobContext job_context(0);
  MutableCFOptions mutable_cf_options = *default_cfd->GetLatestMutableCFOptions();
  std::vector<SequenceNumber> snapshot_seqs;
  SequenceNumber earliest_write_conflict_snapshot;
  SnapshotChecker *snapshot_checker;
  VersionEdit edit;;
  std::vector<FileMetaData> metas;
  dbfull()->mutex()->Lock();
  dbfull()->GetSnapshotContext(&job_context, &snapshot_seqs,
                               &earliest_write_conflict_snapshot, &snapshot_checker);
  std::vector<Slice> empty_boundaries;

  // 写入数据
  Status s = dbfull()->FlushL0CacheToSST(default_cfd, mutable_cf_options, &job_context,
                                         snapshot_seqs, earliest_write_conflict_snapshot,
                                         snapshot_checker, Env::HIGH, kSst_size, empty_boundaries,
                                         edit, metas);
  s = default_cfd->current()->version_set()->LogAndApply(default_cfd, *default_cfd->GetLatestMutableCFOptions(),
                                                         &edit, dbfull()->mutex());
  dbfull()->mutex()->Unlock();

  // Manifest检查
  const auto &l1_files = default_cfd->current()->storage_info()->LevelFiles(1);
  ASSERT_EQ(l1_files.size(), 4);
}

TEST_F(FlushL0CacheTest, InsertAndDeleteFlush) {

  // 为默认列族添加内存存储区和数据
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  ColumnFamilyData *default_cfd = cfh->cfd();
  Options options = Options();
  ImmutableCFOptions ioptions(options);
  ioptions.encode_version_node = false;
  auto memory_arena = new MemArena(default_cfd->internal_comparator(), ioptions);
//  auto memory_arena = new MemArena(default_cfd->internal_comparator(), *default_cfd->ioptions());
  std::map<std::string, std::string> TEST_result = InsertDataAndDelete(memory_arena, 10, 3, 3);
  memory_arena->Ref();
  default_cfd->SetMemoryarena(memory_arena);

  // 创建其他所需参数
  JobContext job_context(0);
  MutableCFOptions mutable_cf_options = *default_cfd->GetLatestMutableCFOptions();
  std::vector<SequenceNumber> snapshot_seqs;
  SequenceNumber earliest_write_conflict_snapshot;
  SnapshotChecker *snapshot_checker;
  VersionEdit edit;;
  std::vector<FileMetaData> metas;
  dbfull()->mutex()->Lock();
  dbfull()->GetSnapshotContext(&job_context, &snapshot_seqs,
                               &earliest_write_conflict_snapshot, &snapshot_checker);
  std::vector<Slice> empty_boundaries;

  // 写入数据
  Status s = dbfull()->FlushL0CacheToSST(default_cfd, mutable_cf_options, &job_context,
                                         snapshot_seqs, earliest_write_conflict_snapshot,
                                         snapshot_checker, Env::HIGH, kApproximateSize, empty_boundaries,
                                         edit, metas);
  s = default_cfd->current()->version_set()->LogAndApply(default_cfd, *default_cfd->GetLatestMutableCFOptions(),
                                                         &edit, dbfull()->mutex());
  dbfull()->mutex()->Unlock();

  // Manifest检查
  const auto &l1_files = default_cfd->current()->storage_info()->LevelFiles(1);
  ASSERT_EQ(l1_files.size(), 1);

  // SST数据插入检查
  ReadRangeDelAggregator range_del_aggregator(&default_cfd->internal_comparator(), kMaxSequenceNumber);
  std::unique_ptr<InternalIterator> it(default_cfd->table_cache()->NewIterator(
      ReadOptions(), env_options_, default_cfd->internal_comparator(), *l1_files[0],
      &range_del_aggregator /* range_del_agg */, mutable_cf_options.prefix_extractor.get(),
      nullptr, nullptr, true /* for_compaction */, nullptr /* arena */,
      false /* skip_filter */, 1));
  for (it->SeekToFirst(); it->Valid(); it->Next()) {
    ASSERT_EQ(it->value().ToString(), TEST_result[it->key().ToString()]);
  }
}

TEST_F(FlushL0CacheTest, LargeMemoryArenaFlush) {

  // 为默认列族添加内存存储区和数据
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  ColumnFamilyData *default_cfd = cfh->cfd();
  Options options = Options();;
  ImmutableCFOptions ioptions(options);
  ioptions.encode_version_node = false;
  auto memory_arena = new MemArena(default_cfd->internal_comparator(), ioptions);
//  auto memory_arena = new MemArena(default_cfd->internal_comparator(), *default_cfd->ioptions());
  InsertData(memory_arena, 40000);
  memory_arena->Ref();
  default_cfd->SetMemoryarena(memory_arena);

  // 创建其他所需参数
  JobContext job_context(0);
  MutableCFOptions mutable_cf_options = *default_cfd->GetLatestMutableCFOptions();
  std::vector<SequenceNumber> snapshot_seqs;
  SequenceNumber earliest_write_conflict_snapshot;
  SnapshotChecker *snapshot_checker;
  VersionEdit edit;;
  std::vector<FileMetaData> metas;
  dbfull()->mutex()->Lock();
  dbfull()->GetSnapshotContext(&job_context, &snapshot_seqs,
                               &earliest_write_conflict_snapshot, &snapshot_checker);
  std::vector<Slice> empty_boundaries;

  // 写入数据
  Status s = dbfull()->FlushL0CacheToSST(default_cfd, mutable_cf_options, &job_context,
                                         snapshot_seqs, earliest_write_conflict_snapshot,
                                         snapshot_checker, Env::HIGH, kApproximateSize, empty_boundaries,
                                         edit, metas);
  s = default_cfd->current()->version_set()->LogAndApply(default_cfd, *default_cfd->GetLatestMutableCFOptions(),
                                                         &edit, dbfull()->mutex());
  dbfull()->mutex()->Unlock();

  // 文件个数检查
  std::vector<LiveFileMetaData> live_files_meta;
  dbfull()->GetLiveFilesMetaData(&live_files_meta);
  ASSERT_EQ(live_files_meta.size(), 4);

  const auto &l1_files = default_cfd->current()->storage_info()->LevelFiles(1);
  int num_entries = 0;
  for (const auto &l1_file : l1_files) {
    num_entries += l1_file->num_entries;
  }
  ASSERT_EQ(l1_files.size(), 4);
  ASSERT_EQ(num_entries, 40000);
}

TEST_F(FlushL0CacheTest, RangeDeletionFlush) {

  // 为默认列族添加内存存储区和数据
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  ColumnFamilyData *default_cfd = cfh->cfd();
  Options options = Options();;
  ImmutableCFOptions ioptions(options);
//  ioptions.encode_version_node = false;
  auto memory_arena = new MemArena(default_cfd->internal_comparator(), ioptions);
//  auto memory_arena = new MemArena(default_cfd->internal_comparator(), *default_cfd->ioptions());
  InsertDataAndRangeDel(memory_arena, 10, 3, 3);
  std::vector<std::string> TEST_result = {genMVCCKey1("100004_test\0").ToString(),
                                    genMVCCKey1("100005_test\0").ToString(),
                                    genMVCCKey1("100009_test\0").ToString(),
                                    genMVCCKey1("100010_test\0").ToString()};
  memory_arena->Ref();
  default_cfd->SetMemoryarena(memory_arena);

  // 创建其他所需参数
  JobContext job_context(0);
  MutableCFOptions mutable_cf_options = *default_cfd->GetLatestMutableCFOptions();
  std::vector<SequenceNumber> snapshot_seqs;
  SequenceNumber earliest_write_conflict_snapshot;
  SnapshotChecker *snapshot_checker;
  VersionEdit edit;;
  std::vector<FileMetaData> metas;
  dbfull()->mutex()->Lock();
  dbfull()->GetSnapshotContext(&job_context, &snapshot_seqs,
                               &earliest_write_conflict_snapshot, &snapshot_checker);
  std::vector<Slice> empty_boundaries;

  // 写入数据
  Status s = dbfull()->FlushL0CacheToSST(default_cfd, mutable_cf_options, &job_context,
                                         snapshot_seqs, earliest_write_conflict_snapshot,
                                         snapshot_checker, Env::HIGH, kApproximateSize, empty_boundaries,
                                         edit, metas);
  s = default_cfd->current()->version_set()->LogAndApply(default_cfd, *default_cfd->GetLatestMutableCFOptions(),
                                                         &edit, dbfull()->mutex());
  dbfull()->mutex()->Unlock();

  // Manifest检查
  const auto &l1_files = default_cfd->current()->storage_info()->LevelFiles(1);
  ASSERT_EQ(l1_files.size(), 1);
  ASSERT_EQ(l1_files[0]->smallest.user_key(), genMVCCKey1("100001_test\0"));
  ASSERT_EQ(l1_files[0]->largest.user_key(), genMVCCKey1("100014_test\0"));

  // SST数据插入检查
  std::unique_ptr<InternalIterator> it(default_cfd->table_cache()->NewIterator(
      ReadOptions(), env_options_, default_cfd->internal_comparator(), *l1_files[0],
      nullptr /* range_del_agg */, mutable_cf_options.prefix_extractor.get(),
      nullptr, nullptr, false /* for_compaction */, nullptr /* arena */,
      false /* skip_filter */, 1));

  int test_data_sum = 0;
  for (it->SeekToFirst(); it->Valid(); it->Next(), ++test_data_sum) {
    ASSERT_EQ(TEST_result[test_data_sum], it->user_key().ToString());
  }
  ASSERT_EQ(test_data_sum, 4);
}

TEST_F(FlushL0CacheTest, RangeDelCutTablesFlush) {

  // 为默认列族添加内存存储区和数据
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  ColumnFamilyData *default_cfd = cfh->cfd();
  Options options = Options();;
  ImmutableCFOptions ioptions(options);
//  ioptions.encode_version_node = false;
  auto memory_arena = new MemArena(default_cfd->internal_comparator(), ioptions);
//  auto memory_arena = new MemArena(default_cfd->internal_comparator(), *default_cfd->ioptions());
  InsertDataAndRangeDel(memory_arena, 50000, 3,
                        10000, 20000, 100000);
  memory_arena->Ref();
  default_cfd->SetMemoryarena(memory_arena);

  // 创建其他所需参数
  JobContext job_context(0);
  MutableCFOptions mutable_cf_options = *default_cfd->GetLatestMutableCFOptions();
  std::vector<SequenceNumber> snapshot_seqs;
  SequenceNumber earliest_write_conflict_snapshot;
  SnapshotChecker *snapshot_checker;
  VersionEdit edit;;
  std::vector<FileMetaData> metas;
  dbfull()->mutex()->Lock();
  dbfull()->GetSnapshotContext(&job_context, &snapshot_seqs,
                               &earliest_write_conflict_snapshot, &snapshot_checker);
  std::vector<Slice> empty_boundaries;

  snapshot_seqs = {125000};
  earliest_write_conflict_snapshot = 125000;

  // 写入数据
  Status s = dbfull()->FlushL0CacheToSST(default_cfd, mutable_cf_options, &job_context,
                                         snapshot_seqs, earliest_write_conflict_snapshot,
                                         snapshot_checker, Env::HIGH, kApproximateSize, empty_boundaries,
                                         edit, metas);
  s = default_cfd->current()->version_set()->LogAndApply(default_cfd, *default_cfd->GetLatestMutableCFOptions(),
                                                         &edit, dbfull()->mutex());
  dbfull()->mutex()->Unlock();

  // Manifest检查
  const auto &l1_files = default_cfd->current()->storage_info()->LevelFiles(1);
  ASSERT_EQ(l1_files.size(), 4);
  int num_entries = 0;
  for (const auto &l1_file : l1_files) {
    num_entries += l1_file->num_entries;
  }
  ASSERT_EQ(num_entries, 35004);
}

TEST_F(FlushL0CacheTest, snapshotsTable) {
  // 为默认列族添加内存存储区和数据
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  ColumnFamilyData *default_cfd = cfh->cfd();
  Options options = Options();;
  ImmutableCFOptions ioptions(options);
//  ioptions.encode_version_node = false;
  auto memory_arena = new MemArena(default_cfd->internal_comparator(), ioptions);
//  auto memory_arena = new MemArena(default_cfd->internal_comparator(), *default_cfd->ioptions());
  InsertDataAndRangeDel(memory_arena, 10, 1,
                        6, 0, 100002);
  memory_arena->Ref();
  default_cfd->SetMemoryarena(memory_arena);

  // 创建其他所需参数
  JobContext job_context(0);
  MutableCFOptions mutable_cf_options = *default_cfd->GetLatestMutableCFOptions();
  std::vector<SequenceNumber> snapshot_seqs;
  SequenceNumber earliest_write_conflict_snapshot;
  SnapshotChecker *snapshot_checker;
  VersionEdit edit;;
  std::vector<FileMetaData> metas;
  dbfull()->mutex()->Lock();
  dbfull()->GetSnapshotContext(&job_context, &snapshot_seqs,
                               &earliest_write_conflict_snapshot, &snapshot_checker);
  std::vector<Slice> empty_boundaries;
  snapshot_seqs = {100005};
  earliest_write_conflict_snapshot = 100005;

  // 写入数据
  Status s = dbfull()->FlushL0CacheToSST(default_cfd, mutable_cf_options, &job_context,
                                         snapshot_seqs, earliest_write_conflict_snapshot,
                                         snapshot_checker, Env::HIGH, 3000, empty_boundaries,
                                         edit, metas);
  s = default_cfd->current()->version_set()->LogAndApply(default_cfd, *default_cfd->GetLatestMutableCFOptions(),
                                                         &edit, dbfull()->mutex());
  dbfull()->mutex()->Unlock();

  const auto &l1_files = default_cfd->current()->storage_info()->LevelFiles(1);

  // SST数据插入检查
  ReadRangeDelAggregator range_del_aggregator(&default_cfd->internal_comparator(), kMaxSequenceNumber);
  std::unique_ptr<InternalIterator> it(default_cfd->table_cache()->NewIterator(
      ReadOptions(), env_options_, default_cfd->internal_comparator(), *l1_files[0],
      &range_del_aggregator /* range_del_agg */, mutable_cf_options.prefix_extractor.get(),
      nullptr, nullptr, true /* for_compaction */, nullptr /* arena */,
      false /* skip_filter */, 1));

  ReadRangeDelAggregator range_del_aggregator2(&default_cfd->internal_comparator(), kMaxSequenceNumber);
  std::unique_ptr<InternalIterator> it2(default_cfd->table_cache()->NewIterator(
      ReadOptions(), env_options_, default_cfd->internal_comparator(), *l1_files[1],
      &range_del_aggregator2 /* range_del_agg */, mutable_cf_options.prefix_extractor.get(),
      nullptr, nullptr, true /* for_compaction */, nullptr /* arena */,
      false /* skip_filter */, 1));

  ASSERT_EQ(l1_files[0]->num_entries, 4);
  ASSERT_EQ(l1_files[1]->num_entries, 4);
  ASSERT_EQ(l1_files[0]->num_deletions, 1);
  ASSERT_EQ(l1_files[1]->num_deletions, 1);
}

TEST_F(FlushL0CacheTest, BoundaiesCutTable) {

  // 为默认列族添加内存存储区和数据
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  ColumnFamilyData *default_cfd = cfh->cfd();
  Options options = Options();;
  ImmutableCFOptions ioptions(options);
//  ioptions.encode_version_node = false;
  auto memory_arena = new MemArena(default_cfd->internal_comparator(), ioptions);
//  auto memory_arena = new MemArena(default_cfd->internal_comparator(), *default_cfd->ioptions());
  InsertData(memory_arena);
  memory_arena->Ref();
  default_cfd->SetMemoryarena(memory_arena);

  // 创建其他所需参数
  JobContext job_context(0);
  MutableCFOptions mutable_cf_options = *default_cfd->GetLatestMutableCFOptions();
  std::vector<SequenceNumber> snapshot_seqs;
  SequenceNumber earliest_write_conflict_snapshot;
  SnapshotChecker *snapshot_checker;
  VersionEdit edit;;
  std::vector<FileMetaData> metas;
  dbfull()->mutex()->Lock();
  dbfull()->GetSnapshotContext(&job_context, &snapshot_seqs,
                               &earliest_write_conflict_snapshot, &snapshot_checker);
  // 添加边界值
  Slice key_slice1 = CreateInternalKeySlice(100003);
  Slice key_slice2 = CreateInternalKeySlice(100006);
  std::vector<Slice> key_boundaries = {key_slice1, key_slice2};
  std::vector<std::pair<std::string, std::string > > TEST_result = {
      {genMVCCKey1("100001_test\0").ToString(), genMVCCKey1("100003_test\0").ToString()},
      {genMVCCKey1("100004_test\0").ToString(), genMVCCKey1("100006_test\0").ToString()},
      {genMVCCKey1("100007_test\0").ToString(), genMVCCKey1("100010_test\0").ToString()},
  };

  // 写入数据
  Status s = dbfull()->FlushL0CacheToSST(default_cfd, mutable_cf_options, &job_context,
                                         snapshot_seqs, earliest_write_conflict_snapshot,
                                         snapshot_checker, Env::HIGH, kApproximateSize, key_boundaries,
                                         edit, metas);
  s = default_cfd->current()->version_set()->LogAndApply(default_cfd, *default_cfd->GetLatestMutableCFOptions(),
                                                         &edit, dbfull()->mutex());
  dbfull()->mutex()->Unlock();

  // Manifest检查
  const auto &l1_files = default_cfd->current()->storage_info()->LevelFiles(1);
  ASSERT_EQ(l1_files.size(), 3);
  int threshold = l1_files.size();
  for (int i = 0; i < threshold; ++i) {
    ASSERT_EQ(TEST_result[i].first, l1_files[i]->smallest.user_key().ToString());
    ASSERT_EQ(TEST_result[i].second, l1_files[i]->largest.user_key().ToString());
  }

  // SST数据插入检查
  ReadRangeDelAggregator range_del_aggregator(&default_cfd->internal_comparator(), kMaxSequenceNumber);
  std::unique_ptr<InternalIterator> it(default_cfd->table_cache()->NewIterator(
      ReadOptions(), env_options_, default_cfd->internal_comparator(), *l1_files[0],
      &range_del_aggregator /* range_del_agg */, mutable_cf_options.prefix_extractor.get(),
      nullptr, nullptr, true /* for_compaction */, nullptr /* arena */,
      false /* skip_filter */, 1));
  int seq = 100000;
  for (it->SeekToFirst(); it->Valid(); it->Next()) {
    ++seq;
    ASSERT_EQ(it->user_key(), genMVCCKey1((std::to_string(seq) + "_test").data()));
    ASSERT_EQ(it->value().ToString()[0], 'a');
  }
}

TEST_F(FlushL0CacheTest, L0CacheInsertFlushTest) {
  Options options = CurrentOptions();
//  options.encode_version_node = false;
  options.rocksdb_l0_cache_flush_l1 = true;
  options.level0_file_num_compaction_trigger = 2;
  Reopen(options);
  for (int i = 0; i < 50; i++) {
    std::string key1 = "a" + ToString(i);
    ASSERT_OK(Put(genMVCCKey1(key1.data()), "pikachu"));
  }
  ASSERT_EQ("pikachu", Get(genMVCCKey1("a11").ToString()));
  FlushOptions flush_options;
  ASSERT_OK(dbfull()->Flush(flush_options));
  ASSERT_EQ(NumTableFilesAtLevel(0), 1);
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  MemArena *memArena = cfh->cfd()->memoryarena();
  std::string key_tmp = "a" + ToString(15);
  Slice key_find(genMVCCKey1(key_tmp.data()));
  int len = (int) key_find.size() + 8;
  char find[len];
  memcpy(find, key_find.data(), key_find.size());
  uint64_t packed = PackSequenceAndType(100000, kTypeValue);   /////
  EncodeFixed64(find + key_find.size(), packed);

//  memset(find + key_find.size(), 255, 8);
  Slice key(find, len);
  ASSERT_EQ(memArena->Seek(key)->Valid(), true);
  for (int i = 0; i < 50; i++) {
    std::string key2 = "a" + ToString(i);
    ASSERT_OK(Put(genMVCCKey1(key2.data()), "pikachu"));
  }
  ASSERT_OK(dbfull()->Flush(flush_options));
  ASSERT_EQ(NumTableFilesAtLevel(0), 0);
  ASSERT_EQ(NumTableFilesAtLevel(1), 1);
}

TEST_F(FlushL0CacheTest, L0CacheInsertFlushGetTest) {
  Options options = CurrentOptions();
//  options.encode_version_node = false;
  options.rocksdb_l0_cache_flush_l1 = true;
  options.level0_file_num_compaction_trigger = 2;
  ReadOptions readOptions;
  Reopen(options);
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  for (int i = 0; i < 50; i++) {
    std::string key1 = "a" + ToString(i);
    ASSERT_OK(Put(genMVCCKey1(key1.data()), "pikachu" + ToString(i)));
  }
  Slice key_find("a15");
  PinnableSlice value_get;
  Slice get_key = genMVCCKey1(key_find.data());
  // Get from memtable
  auto s = dbfull()->GetImpl(readOptions, cfh, get_key,  &value_get);
  ASSERT_OK(s.OK());
  ASSERT_EQ("pikachu15", *value_get.GetSelf());

  FlushOptions flush_options;
  ASSERT_OK(dbfull()->Flush(flush_options));
  ASSERT_EQ(NumTableFilesAtLevel(0), 1);
  //MemoryArena *memArena = cfh->cfd()->memoryarena();

  PinnableSlice value_get1;
  // Get from memArena
  s = dbfull()->GetImpl(readOptions, cfh, get_key,  &value_get1);
  ASSERT_TRUE(s.ok());
  ASSERT_EQ("pikachu15", *value_get1.GetSelf());

  for (int i = 0; i < 50; i++) {
    std::string key2 = "a" + ToString(i);
    ASSERT_OK(Put(genMVCCKey1(key2.data()), "pikachu"));
  }
  ASSERT_OK(dbfull()->Flush(flush_options));
  ASSERT_EQ(NumTableFilesAtLevel(0), 0);
  ASSERT_EQ(NumTableFilesAtLevel(1), 1);
  PinnableSlice value_get2;
  // Get from L1
  s = dbfull()->GetImpl(readOptions, cfh, get_key,  &value_get2);
  ASSERT_TRUE(s.ok());
  ASSERT_EQ("pikachu", value_get2.ToString());
}

TEST_F(FlushL0CacheTest, L0CacheInsertFlushMultiTest) {
  Options options = CurrentOptions();
//  options.encode_version_node = false;
  options.rocksdb_l0_cache_flush_l1 = true;
  options.level0_file_num_compaction_trigger = 2;
  Reopen(options);
  for (int i = 0; i < 50; i++) {
    std::string key1 = "a" + ToString(i);
    ASSERT_OK(Put(genMVCCKey1(key1.data()), "pikachu"));
  }
  ASSERT_EQ("pikachu", Get(genMVCCKey1("a11").ToString()));
  FlushOptions flush_options;
  ASSERT_OK(dbfull()->Flush(flush_options));
  ASSERT_EQ(NumTableFilesAtLevel(0), 1);
  for (int i = 0; i < 50; i++) {
    std::string key2 = "b" + ToString(i);
    ASSERT_OK(Put(genMVCCKey1(key2.data()), "pikachu"));
  }
  ASSERT_OK(dbfull()->Flush(flush_options));
  ASSERT_EQ(NumTableFilesAtLevel(0), 0);
  ASSERT_EQ(NumTableFilesAtLevel(1), 1);
  for (int i = 0; i < 50; i++) {
    std::string key1 = "c" + ToString(i);
    ASSERT_OK(Put(genMVCCKey1(key1.data()), "pikachu"));
  }
  ASSERT_OK(dbfull()->Flush(flush_options));
  ASSERT_EQ(NumTableFilesAtLevel(0), 1);
  for (int i = 0; i < 50; i++) {
    std::string key2 = "d" + ToString(i);
    ASSERT_OK(Put(genMVCCKey1(key2.data()), "pikachu"));
  }
  ASSERT_OK(dbfull()->Flush(flush_options));
  ASSERT_EQ(NumTableFilesAtLevel(0), 0);
  for (int i = 0; i < 50; i++) {
    std::string key1 = "e" + ToString(i);
    ASSERT_OK(Put(genMVCCKey1(key1.data()), "pikachu"));
  }
  ASSERT_OK(dbfull()->Flush(flush_options));
  ASSERT_EQ(NumTableFilesAtLevel(0), 1);
  for (int i = 0; i < 50; i++) {
    std::string key2 = "f" + ToString(i);
    ASSERT_OK(Put(genMVCCKey1(key2.data()), "pikachu"));
  }
  ASSERT_OK(dbfull()->Flush(flush_options));
  ASSERT_EQ(NumTableFilesAtLevel(0), 0);
}

TEST_F(FlushL0CacheTest, L0CacheRangeDeletionFlushGetTest) {
  Options options = CurrentOptions();
//  options.encode_version_node = false;
  options.rocksdb_l0_cache_flush_l1 = true;
  options.level0_file_num_compaction_trigger = 2;
  ReadOptions readOptions;
  Reopen(options);
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  for (int i = 0; i < 50; i++){
    std::string key =  "a" + ToString(i);
    ASSERT_OK(Put(genMVCCKey1(key.data()), "pikachu"));
  }
  ASSERT_OK(dbfull()->DeleteRange(WriteOptions(), dbfull()->DefaultColumnFamily(),
                                        genMVCCKey1("a10"), genMVCCKey1("a20")));
  ASSERT_EQ("NOT_FOUND", Get(genMVCCKey1("a11").ToString()));
  Slice key_find("a11");
  PinnableSlice value_get;
  Slice get_key = genMVCCKey1(key_find.data());
  // Get from memtable
  auto s = dbfull()->GetImpl(readOptions, cfh, get_key,  &value_get);
  ASSERT_TRUE(s.IsNotFound());
  FlushOptions flush_options;
  ASSERT_OK(dbfull()->Flush(flush_options));
  ASSERT_EQ(NumTableFilesAtLevel(0), 1);
  // Get from memArena
  s = dbfull()->GetImpl(readOptions, cfh, get_key,  &value_get);
  ASSERT_TRUE(s.IsNotFound());

  for (int i = 0; i < 50; i++){
    std::string key1 =  "b" + ToString(i);
    ASSERT_OK(Put(genMVCCKey1(key1.data()), "pikachu"));
  }
  ASSERT_OK(dbfull()->Flush(flush_options));
  ASSERT_EQ(NumTableFilesAtLevel(0), 0);
  ASSERT_EQ(NumTableFilesAtLevel(1), 1);
}

//TEST_F(FlushL0CacheTest, L0CacheLargeDataTest) {
//  Options options = CurrentOptions();
////  options.encode_version_node = false;
//  options.rocksdb_l0_cache_flush_l1 = true;
//  options.level0_file_num_compaction_trigger = 2;
//  options.max_background_flushes = 1;
//
//  ReadOptions readOptions;
//  Reopen(options);
//  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
//  for(int i = 0;i < 30000;i++){
//    std::string str_key = "key" + ToString(i);
//    const char* key = str_key.data();
//    ASSERT_OK(Put(genMVCCKey1(key), std::string("T", 1024)));
//  }
//  ASSERT_OK(dbfull()->DeleteRange(WriteOptions(), dbfull()->DefaultColumnFamily(),
//                                  genMVCCKey1("key17000"), genMVCCKey1("key18000")));
//  ASSERT_EQ(std::string("T", 1024), Get(genMVCCKey1("key2333").ToString()));
//  ASSERT_EQ("NOT_FOUND", Get(genMVCCKey1("a17173").ToString()));
//
//  int level = NumTableFilesAtLevel(0);
//  ASSERT_EQ(level, 1);
//  Slice key_find("key18181");
//  PinnableSlice value_get;
//  Slice get_key = genMVCCKey1(key_find.data());
//  // Get from memtable
//  auto s = dbfull()->GetImpl(readOptions, cfh, get_key,  &value_get);
//  ASSERT_TRUE(s.ok());
//  // Get from memArena
//  key_find = "key17173";
//  get_key = genMVCCKey1(key_find.data());
//  s = dbfull()->GetImpl(readOptions, cfh, get_key,  &value_get);
//  ASSERT_TRUE(s.IsNotFound());
//
//  for(int i = 0;i < 20000;i++){
//    std::string str_key = "key" + ToString(i+20000);
//    const char* key = str_key.data();
//    ASSERT_OK(Put(genMVCCKey1(key), std::string("T", 1024)));
//  }
//  ASSERT_EQ(NumTableFilesAtLevel(0), 0);
//  // Get from sst
//  key_find = "key16162";
//  get_key = genMVCCKey1(key_find.data());
//  s = dbfull()->GetImpl(readOptions, cfh, get_key,  &value_get);
//  ASSERT_TRUE(s.ok());
//}

TEST_F(FlushL0CacheTest, L0CacheHurgeDataTest) {
  Options options = CurrentOptions();
//  options.encode_version_node = false;
  options.rocksdb_l0_cache_flush_l1 = true;
  options.level0_file_num_compaction_trigger = 2;
  options.max_background_flushes = 1;

  ReadOptions readOptions;
  Reopen(options);
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  for(int i = 0;i < 1000000;i++){
    std::string str_key = "key" + ToString(i);
    const char* key = str_key.data();
    ASSERT_OK(Put(genMVCCKey1(key), std::string("T", 1024)));
  }
}

//TEST_F(FlushL0CacheTest, L0CacheMultiThreadTest) {
//  Options options = CurrentOptions();
////  options.encode_version_node = false;
//  options.rocksdb_l0_cache_flush_l1 = true;
//  options.level0_file_num_compaction_trigger = 2;
//  options.max_background_flushes = 1;
//
//  ReadOptions readOptions;
//  Reopen(options);
//  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
//  std::thread threads[10];
//  for (int id = 0; id < 10; ++id)
//    threads[id] = std::thread([&](int n){
//      for(int i = 0; i < 3000; ++i){
//        std::string key = "key" + ToString(n * 10000 + i);
//        ASSERT_OK(Put(genMVCCKey1(key.data()), std::string("T", 1024)));
//      }
//      }, id);
//  for (auto &thread : threads){
//    thread.join();
//  }
//
//  for(int i = 0; i < 10; ++i) {
//    for (int j = 0; j < 3000; ++j) {
//      std::string key = "key" + ToString(i * 10000 + j);
//      //ASSERT_EQ(Get(genMVCCKey1(key.data()).ToString()), std::string("T", 1024));
//      Slice key_find(key.data());
//      PinnableSlice value_get;
//      Slice get_key = genMVCCKey1(key_find.data());
//      // Get from memtable
//      auto s = dbfull()->GetImpl(readOptions, cfh, get_key,  &value_get);
//      ASSERT_TRUE(s.ok());
//    }
//  }
//
//  ASSERT_EQ(NumTableFilesAtLevel(0), 1);
//
//  for (int id = 0; id < 10; ++id)
//    threads[id] = std::thread([&](int n){
//        for(int i = 0; i < 1000; ++i){
//          std::string key = "key" + ToString(n * 20000 + i );
//          ASSERT_OK(Put(genMVCCKey1(key.data()), std::string("T", 1024)));
//        }
//    }, id);
//  for (auto &thread : threads){
//    thread.join();
//  }
//
//  for(int i = 0; i < 10; ++i) {
//    for (int j = 0; j < 1000; ++j) {
//      std::string key = "key" + ToString(i * 20000 + j );
//      Slice key_find(key.data());
//      PinnableSlice value_get;
//      Slice get_key = genMVCCKey1(key_find.data());
//      // Get from sst
//      auto s = dbfull()->GetImpl(readOptions, cfh, get_key,  &value_get);
//      ASSERT_TRUE(s.ok());
//    }
//  }
//
//  ASSERT_EQ(NumTableFilesAtLevel(0), 0);
//}

TEST_F(FlushL0CacheTest, L0CacheMultiThreadHugeInsertTest) {
  Options options = CurrentOptions();
//  options.encode_version_node = false;
  options.rocksdb_l0_cache_flush_l1 = true;
  options.level0_file_num_compaction_trigger = 10;
  options.max_background_flushes = 1;
  options.max_background_compactions = -1;

  ReadOptions readOptions;
  Reopen(options);
  auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
  std::thread threads[100];
  for (int id = 0; id < 100; ++id)
    threads[id] = std::thread([&](int n){
      for(int i = 0; i < 5000; ++i){
        std::string key = "key" + ToString(n * 10000 + i);
        ASSERT_OK(Put(genMVCCKey1(key.data()), std::string("T", 1024)));
      }
      }, id);
  for (auto &thread : threads){
     thread.join();
  }


}

TEST_F(FlushL0CacheTest, IteratorTest) {
    Options options = CurrentOptions();
//    options.encode_version_node = false;
    options.rocksdb_l0_cache_flush_l1 = true;
    options.level0_file_num_compaction_trigger = 2;
    ReadOptions readOptions;
    CreateAndReopenWithCF({"pikachu"}, options);
    auto cfh = reinterpret_cast<ColumnFamilyHandleImpl *>(dbfull()->DefaultColumnFamily());
    MemArena *memArena = cfh->cfd()->memoryarena();
    for (int i = 0; i < 50; i++) {
        std::string key1 = "a" + ToString(i);
        ASSERT_OK(Put(1, genMVCCKey1(key1.data()), "pikachu"));
    }
    //Flush(1);
    ASSERT_OK(Flush(1));
    ASSERT_EQ(NumTableFilesAtLevel(0, 1), 1);
    std::string a = "a15";
    Slice key_find(genMVCCKey1(a.data()));
    int len = (int) key_find.size() + 8 + 1;
    //uint32_t keySize = VarintLength(len) + len;
    char find[len];
    memset(find, '\0', len);
    memcpy(find, key_find.data(), key_find.size());
    memset(find + len - 8, 255, 8);
    Slice key(find, len);
    Slice get_key = genMVCCKey1(key_find.data());

      // memory_arena
    auto* it = db_->NewIterator(readOptions, handles_[1]);
    it->Seek(get_key);
    ASSERT_TRUE(it->Valid());
    for(it->SeekToFirst(); it->Valid(); it->Next()) {
        ASSERT_TRUE(it->Valid());
        ASSERT_EQ(it->value().size(), 7);
    }
    delete it;

    for (int i = 0; i < 50; i++) {
        std::string key2 = "a" + ToString(i);
        ASSERT_OK(Put(1, genMVCCKey1(key2.data()), "pikachu" ));
    }
    ASSERT_OK(Flush(1));
    ASSERT_EQ(NumTableFilesAtLevel(0, 1), 0);
    ASSERT_EQ(NumTableFilesAtLevel(1, 1), 1);
    // FLUSH L1, memArena seek() expects false
    InternalIterator* iter_memarena = memArena->NewIterator(false);
    iter_memarena->Seek(key);
    ASSERT_EQ(iter_memarena->Valid(), false);
    delete iter_memarena;
      // L1 seek
    auto* it1 = db_->NewIterator(readOptions, handles_[1]);

    it1->Seek(get_key);
    ASSERT_TRUE(it1->Valid());
    ASSERT_EQ(it1->value().ToString(), "pikachu");

    for(it1->SeekToFirst(); it1->Valid(); it1->Next()) {
        ASSERT_TRUE(it1->Valid());
      ASSERT_EQ(it1->value().ToString(), "pikachu");
    }
    delete it1;


}

TEST_F(FlushL0CacheTest, L0CacheRangeDeletionFlushIteratorTest) {
      Options options = CurrentOptions();
      //options.encode_version_node = false;
      options.rocksdb_l0_cache_flush_l1 = true;
      options.level0_file_num_compaction_trigger = 2;
      ReadOptions readOptions;
      CreateAndReopenWithCF({"pikachu"}, options);
      for (int i = 0; i < 50; i++){
        std::string key =  "a" + ToString(i);
        ASSERT_OK(Put(1, genMVCCKey1(key.data()), "pikachu"));
      }
      ASSERT_OK(dbfull()->DeleteRange(WriteOptions(), dbfull()->DefaultColumnFamily(),
                                      genMVCCKey1("a10"), genMVCCKey1("a20")));
      ASSERT_EQ("NOT_FOUND", Get(genMVCCKey1("a11").ToString()));
      Slice key_find("a11");
      PinnableSlice value_get;
      Slice get_key = genMVCCKey1(key_find.data());
      ASSERT_OK(Flush(1));
      ASSERT_EQ(NumTableFilesAtLevel(0, 1), 1);
      // Get from memArena
      auto* it = db_->NewIterator(readOptions, handles_[1]);
      // memory_arena
      it->Seek(get_key);
      ASSERT_TRUE(it->Valid());
      for(it->SeekToFirst(); it->Valid(); it->Next()) {
        ASSERT_TRUE(it->Valid());
        ASSERT_EQ(it->value().size(), 7);
      }
      delete it;

      for (int i = 0; i < 50; i++){
        std::string key1 =  "b" + ToString(i);
        ASSERT_OK(Put(1, genMVCCKey1(key1.data()), "pikachu"));
      }
      ASSERT_OK(Flush(1));
      ASSERT_EQ(NumTableFilesAtLevel(0, 1), 0);
      ASSERT_EQ(NumTableFilesAtLevel(1, 1), 1);

      // L1
      auto* it1 = db_->NewIterator(readOptions, handles_[1]);

      it1->Seek(get_key);
      ASSERT_TRUE(it1->Valid());
      ASSERT_EQ(it1->value().ToString(), "pikachu");
      for(it1->SeekToFirst(); it1->Valid(); it1->Next()) {
        ASSERT_TRUE(it1->Valid());
        ASSERT_EQ(it1->value().ToString(), "pikachu");
      }
      delete it1;
    }

}  // namespace rocksdb

int main(int argc, char **argv) {
  rocksdb::port::InstallStackTraceHandler();
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
