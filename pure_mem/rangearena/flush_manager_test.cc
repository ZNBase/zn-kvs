// Copyright (c) 2020-present,  INSPUR Co, Ltd.  All rights reserved.
// This source code is licensed under Apache 2.0 License.

#include "flush_manager.h"

#include <memory>
#include <string>

#include "db/column_family.h"
#include "db/flush_job.h"
#include "db/memtable.h"
#include "db/version_set.h"
#include "pure_mem/std_out_logger.h"
#include "rocksdb/cache.h"
#include "rocksdb/write_buffer_manager.h"
#include "table/mock_table.h"
#include "util/file_reader_writer.h"
#include "util/string_util.h"
#include "util/testharness.h"
#include "util/testutil.h"

namespace rocksdb {
class FlushManagerTest : public testing::Test {
 public:
  FlushManagerTest()
      : env_(Env::Default()),
        dbname_(test::PerThreadDBPath("flush_job_test")),
        info_log_(new StdOutLogger(InfoLogLevel::DEBUG_LEVEL)),
        comparator_(InternalKeyComparator(BytewiseComparator())){
    Options options;
    // Create a MemTable
    options.memtable_factory = std::make_shared<PureMemFactory>();
    options.pureMemTable = true;
    options.cf_paths.emplace_back(dbname_, 0);
    options_ = options;

    ioptions_ = ImmutableCFOptions(options);
    moptions_ = MutableCFOptions(options);
    write_buffer_manager_ = std::make_shared<WriteBufferManager>(options.db_write_buffer_size);
    mem_tracker_ = std::make_shared<AllocTracker>(write_buffer_manager_.get());
    arena_ = std::make_shared<ConcurrentArena>(moptions_.arena_block_size,
                                               (write_buffer_manager_->enabled() ||write_buffer_manager_->cost_to_cache())
                                               ? mem_tracker_.get() : nullptr, moptions_.memtable_huge_page_size);
  }

  Env* env_;
  std::string dbname_;
  std::shared_ptr<Logger> info_log_;
  MemTable::KeyComparator comparator_;

  Options options_;
  ImmutableCFOptions ioptions_;
  MutableCFOptions moptions_;
  std::shared_ptr<WriteBufferManager> write_buffer_manager_;
  std::shared_ptr<AllocTracker> mem_tracker_;
  std::shared_ptr<ConcurrentArena> arena_;

  MemTableRep* TEST_CreateMemtatableRep();
};

MemTableRep* FlushManagerTest::TEST_CreateMemtatableRep(){
  return options_.memtable_factory->CreateMemTableRep(
      comparator_, arena_.get(), moptions_.prefix_extractor.get(), info_log_.get());
}



} // namespace rocksdb


int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
