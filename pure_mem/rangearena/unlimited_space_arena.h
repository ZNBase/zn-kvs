// Copyright (c) 2020-present,  INSPUR Co, Ltd.  All rights reserved.
// This source code is licensed under Apache 2.0 License.

#include <iostream>

namespace rocksdb {

class UnlimitedArena : public Allocator {
 public:
  explicit UnlimitedArena(AllocTracker *tracker = nullptr)
      : tracker_(tracker){}

  ~UnlimitedArena() override {
    for (auto addr : addrs_) {
      delete[] addr;
    }
  }

  char *Allocate(size_t bytes, const Slice &key = nullptr, void *node = nullptr,
                 void **rangearena = nullptr) override {
    assert(bytes > 0);
    char* addr_ = new char[bytes];
    addrs_.push_back(addr_);
    return addr_;
  }

  char *AllocateAligned(size_t bytes, size_t huge_page_size = 2,
                        Logger *logger = nullptr) override {
    char* addr_ = new char[bytes];
    addrs_.push_back(addr_);
    return addr_;
  }

  size_t BlockSize() const override { return 0; }

 private:
  AllocTracker *tracker_;
  std::vector<char *> addrs_;
};
}  // namespace rocksdb
