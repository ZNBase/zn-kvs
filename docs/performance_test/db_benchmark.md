# DB Benchmark Test

## (18/3/2021)

## Test Environment

个人电脑，其配置为 8核CPU， 64G内存。

## DB Benchmark

### Introduction

使用Rocksdb 自带的 db_bench 工具，改造增加参数 pureMemTable 以支持 zn-kvs 。

基本的运行命令为： 
~~~shell
./db_bench --benchmarks="fillrandom,readrandom" --compression_type=none --num=1000000 --key_size=20 --value_size=1024 --threads=1 --use_direct_io_for_flush_and_compaction=true--pureMemTable=??
~~~

### KV data num diff

* 关于不同数据量下，Rocksdb 与 zn-kvs 的读写速度统计如下：

![bench_diff_recs](images/bench_diff_records.png)


### Multi Threads

* 不同数量线程并发写入时的写入操作耗时

![bench_diff_recs](images/bench_diff_mulithread_w.png)

* 不同数量线程并发查询时的查询操作耗时

![bench_diff_recs](images/bench_diff_mulithread_r.png)


结论：在数据量1G的场景下，zn-kvs与Rocksdb插入操作耗时相近，zn-kvs的查询速度为Rocksdb的两倍。


### key_size

* 不同key长度写入时的写入操作耗时

![bench_diff_key_size_w](images/bench_diff_key_size_w.png)


* 不同key长度查询时的查询操作耗时

![bench_diff_key_size_r](images/bench_diff_key_size_r.png)


结论：
1. key长度对插入逻辑影响不大。 但是对 rocksdb的查询逻辑有较大的影响，key越长，查询耗时会越多些。
2. 在 1G 数据量下， zn-kvs 的写入速度比rocksdb差约 20%， 查询速度 比rocksdb 快3倍。


### use_direct_reads

* 开启直接io读写的查询操作耗时

![bench_use_direct_reads_r](images/bench_use_direct_reads_r.png)


结论：开启直接IO读写，读性能减低 300%。但是对 插入操作没有影响。

### value_size

* 不同value长度写入时的写入操作耗时

![bench_diff_value_size_w](images/bench_diff_value_size_w.png)


* 不同value长度查询时的查询操作耗时

![bench_diff_value_size_r](images/bench_diff_value_size_r.png)


结论：
（1）记录的长度对插入无直接影响。 
（2）对rocksdb 的查询影响很大， 主要是缓存空间不足，需要不断加载新的文件块。